#!/bin/sh

mkdir certs
cd certs

echo -e "\n\n\ngenerating ca for sign server\n\n\n"
openssl genrsa -out sign-server-ca.key 2048

openssl req -x509 -new -nodes \
            -key sign-server-ca.key -sha256 \
            -days 3650 -out sign-server-ca.crt

echo -e "\n\n\ngenerating ca for storage access\n\n\n"
openssl genrsa -out access-ca.key 2048

openssl req -x509 -new -nodes \
            -key access-ca.key -sha256 \
            -days 3650 -out access-ca.crt
