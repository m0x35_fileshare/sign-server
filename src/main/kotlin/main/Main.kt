package main

import org.sql2o.Sql2o
import spark.kotlin.Http
import spark.kotlin.ignite

fun main(args: Array<String>) {

    var cfgPath = "./cfg.ini"
    if (args.isNotEmpty())
        cfgPath = args[0]
    val cfg = Config(cfgPath)

    val dbConnectionProvider = Sql2oConnectionProvider(provideSql2oDb(cfg))
    val signRepository = Sql2oSignRequestRepository(dbConnectionProvider)
    val signService = SignRequestService(signRepository,
                                         caCertPath = cfg.getCaCertPath(),
                                         caKeyPath = cfg.getCaKeyPath())

    val signRequestHandler = SignRequestsHandler(signService)
    var http: Http = ignite()
    cfg.getListenPort()?.let { http = http.port(it) }

    http.post("/sign-request/:requestId") {
        signRequestHandler.handle(request, response)
    }
}

fun provideSql2oDb(cfg: Config): Sql2o {
    return Sql2o(cfg.getDbConnectionStr(), cfg.getDbUsername(), cfg.getDbPassword(), Sql2oQuirks())
}
