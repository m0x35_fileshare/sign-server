package main

import java.io.FileInputStream
import java.util.*

class Config(cfgPath: String) {

    private val cfgFile = Properties()

    init {
        cfgFile.load(FileInputStream(cfgPath))
    }

    fun getDbConnectionStr(): String = cfgFile.getProperty("db_connection")
    fun getDbPassword(): String = cfgFile.getProperty("db_password")
    fun getDbUsername(): String = cfgFile.getProperty("db_username")
    fun getCaCertPath(): String = cfgFile.getProperty("ca_cert_path")
    fun getCaKeyPath(): String = cfgFile.getProperty("ca_key_path")
    fun getListenPort(): Int? = cfgFile.getProperty("port").toIntOrNull()
}