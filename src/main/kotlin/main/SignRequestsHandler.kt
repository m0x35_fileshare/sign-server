package main

import spark.Request
import spark.Response
import java.lang.Exception
import java.util.*

class SignRequestsHandler(private val signRequestService: SignRequestService) {

    fun handle(req: Request, response: Response): Any {
        response.type("text/plain")

        try {
            val requestId = UUID.fromString(req.params(":requestId"))

            if (!signRequestService.isRequestIdValid(requestId)) {
                response.status(HttpStatus.NOT_FOUND.status)
                return "Request id doesn't exist\n"
            }

            val data = req.bodyAsBytes()
            return signRequestService.sign(requestId, data)
        } catch (e: Exception) {
            e.printStackTrace()
            response.status(HttpStatus.INTERNAL_SERVER_ERROR.status)
            return e.localizedMessage + '\n'
        }
    }
}