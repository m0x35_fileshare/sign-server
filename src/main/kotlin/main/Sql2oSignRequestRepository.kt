package main

import java.nio.ByteBuffer
import java.nio.ByteOrder
import java.util.*

class Sql2oSignRequestRepository(private val connectionProvider: Sql2oConnectionProvider) :
    SignRequestRepository {

    private val table = "storage_sign_request"

    override fun findById(requestId: UUID): Optional<SignRequest> {
        val conn = connectionProvider.getConnection()

        val sql = "select request_id, storage_id from $table " +
                "where request_id=:request_id;"

        val query = conn
            .createQuery(sql)
            .addParameter("request_id", requestId)
            .setColumnMappings(mapOf("request_id" to "requestId", "storage_id" to "storageId"))

        println(query.toString())

        val firstItem = query
            .executeAndFetchFirst(SignRequest::class.java)

        return if (firstItem != null) Optional.of(firstItem) else Optional.empty()
    }

    override fun remove(requestId: UUID) {
        val conn = connectionProvider.getConnection()

        val sql = "delete from $table " +
                "where request_id=:request_id;"

        println(sql)

        conn
            .createQuery(sql)
            .addParameter("request_id", requestId)
            .executeUpdate()
    }
}