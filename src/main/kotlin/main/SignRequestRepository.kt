package main

import java.util.*

interface SignRequestRepository {
    fun findById(requestId: UUID): Optional<SignRequest>
    fun remove(requestId: UUID)
}