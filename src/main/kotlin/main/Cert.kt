package main

import java.util.UUID

data class Cert(
    var storageInstanceId: UUID? = null,
    var cert: Iterable<Byte>? = null
)