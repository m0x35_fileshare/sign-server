package main

import java.nio.charset.Charset
import java.util.*
import java.util.concurrent.TimeUnit

class SignRequestService(
    private val signRequestRepo: SignRequestRepository,
    private val caCertPath: String,
    private val caKeyPath: String
) {

    class InvalidRequestIdException : Exception("Invalid sign request id")
    class SignError(error: String) : Exception(error)

    fun isRequestIdValid(signRequestId: UUID) = signRequestRepo.findById(signRequestId).isPresent

    fun sign(signRequestId: UUID, signRequestData: ByteArray): String {
        val signRequest = signRequestRepo.findById(signRequestId)
        if (!signRequest.isPresent)
            throw InvalidRequestIdException()

        val result = signCsr(signRequestData)
        signRequestRepo.remove(signRequestId)

        return result
    }

    private fun signCsr(signRequestData: ByteArray): String {

        val days = 3600
        val process = Runtime.getRuntime().exec(
            "openssl x509 " +
                    "-req " +
                    " -CAcreateserial " +
                    "-days $days " +
                    "-CA $caCertPath " +
                    "-CAkey $caKeyPath " +
                    "-sha256"
        )
        println(signRequestData.toString())
        process.outputStream.write(signRequestData)
        process.outputStream.write('\n'.toByte().toInt())
        process.outputStream.flush()
        if (!process.waitFor(2500, TimeUnit.MILLISECONDS))
            throw SignError("Sign timeout")

        val exitValue = process.exitValue()
        if (exitValue != 0) {
            val errorString = process.errorStream.reader().readText()
            throw SignError("Signing process exited with code $exitValue: $errorString")
        }

        return process.inputStream.reader().readText()
    }
}

