package main

import java.util.UUID

data class SignRequest (
    var requestId: UUID? = null,
    var storageId: UUID? = null
)